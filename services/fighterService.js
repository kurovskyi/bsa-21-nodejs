const { FighterRepository } = require('../repositories/fighterRepository');

const { ExtendedError } = require('../utils/error.utils');
class FighterService {
  async getAll() {
    try {
      const result = FighterRepository.getAll();

      return result;
    } catch (err) {
      if (!err.message) err.message = 'Fighters getting error.';
      throw err;
    }
  }

  async getOne(data) {
    try {
      const item = FighterRepository.getOne(data);
      if (!item) throw new ExtendedError('Fighter not found.', true);

      return item;
    } catch (err) {
      if (!err.message) err.message = 'Fighter getting error.';
      throw err;
    }
  }

  async getOneById(id) {
    return await this.getOne({ id });
  }

  async createOne(data) {
    try {
      const { name } = data;

      let existingFighter;
      try {
        existingFighter = await this.getOne({ name });
      } catch {}
      if (existingFighter) throw new Error('Fighter already exists.');

      const item = FighterRepository.create(data);
      if (!item) throw new Error();

      return item;
    } catch (err) {
      if (!err.message) err.message = 'Fighter creating error.';
      throw err;
    }
  }

  async updateOne(id, data) {
    try {
      try {
        await this.getOneById(id);
      } catch {
        throw new ExtendedError('Fighter to update not found.', true);
      }

      const item = FighterRepository.update(id, data);

      return item;
    } catch (err) {
      if (!err.message) err.message = 'Fighter updating error.';
      throw err;
    }
  }

  async deleteOne(id) {
    try {
      try {
        await this.getOneById(id);
      } catch {
        throw new ExtendedError('Fighter to delete not found.', true);
      }

      const item = FighterRepository.delete(id);

      return item;
    } catch (err) {
      if (!err.message) err.message = 'Fighter deleting error.';
      throw err;
    }
  }
}

module.exports = new FighterService();
