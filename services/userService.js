const { UserRepository } = require('../repositories/userRepository');

const { ExtendedError } = require('../utils/error.utils');

class UserService {
  async getAll() {
    try {
      const result = UserRepository.getAll();

      return result;
    } catch (err) {
      if (!err.message) err.message = 'Users getting error.';
      throw err;
    }
  }

  async getOne(data) {
    try {
      const item = UserRepository.getOne(data);
      if (!item) throw new ExtendedError('User not found.', true);

      return item;
    } catch (err) {
      if (!err.message) err.message = 'User getting error.';
      throw err;
    }
  }

  async getOneById(id) {
    return await this.getOne({ id });
  }

  async createOne(data) {
    try {
      const { email, phoneNumber } = data;

      let existingUser;
      try {
        existingUser = await this.getOne({ email, phoneNumber });
      } catch {}
      if (existingUser) throw new Error('User already exists.');

      const item = UserRepository.create(data);
      if (!item) throw new Error();

      return item;
    } catch (err) {
      if (!err.message) err.message = 'User creating error.';
      throw err;
    }
  }

  async updateOne(id, data) {
    try {
      try {
        await this.getOneById(id);
      } catch {
        throw new ExtendedError('User to update not found.', true);
      }

      const item = UserRepository.update(id, data);

      return item;
    } catch (err) {
      if (!err.message) err.message = 'User updating error.';
      throw err;
    }
  }

  async deleteOne(id) {
    try {
      try {
        await this.getOneById(id);
      } catch {
        throw new ExtendedError('User to delete not found.', true);
      }

      const item = UserRepository.delete(id);

      return item;
    } catch (err) {
      if (!err.message) err.message = 'User deleting error.';
      throw err;
    }
  }
}

module.exports = new UserService();
