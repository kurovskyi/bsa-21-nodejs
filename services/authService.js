const UserService = require('./userService');

const { ExtendedError } = require('../utils/error.utils');

class AuthService {
  async login(userData) {
    try {
      const user = await UserService.getOne(userData);

      if (!user) throw new ExtendedError('User not found', true);

      return user;
    } catch (err) {
      if (!err.message) err.message = 'User login error.';
      throw err;
    }
  }
}

module.exports = new AuthService();
