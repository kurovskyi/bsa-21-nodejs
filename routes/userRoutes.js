const { Router } = require('express');
const UserService = require('../services/userService');
const {
  createUserValid,
  updateUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
  '/',
  async (_, res, next) => {
    try {
      const data = await UserService.getAll();
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  async (req, res, next) => {
    try {
      const data = await UserService.getOneById(req.params.id);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/',
  createUserValid,
  async (req, res, next) => {
    try {
      const data = await UserService.createOne(req.body);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  updateUserValid,
  async (req, res, next) => {
    try {
      const data = await UserService.updateOne(req.params.id, req.body);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  async (req, res, next) => {
    try {
      const data = await UserService.deleteOne(req.params.id);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
