const { ExtendedError } = require('../utils/error.utils');

const responseMiddleware = (_, res, next) => {
  const { err, data } = res;

  let status = 200;

  if (!err && data) res.status(status).send(data);
  else {
    if (err.isNotFound) status = 404;
    else status = 400;

    res.status(status).send(ExtendedError.generateResponseError(err.message));
  }

  next();
};

exports.responseMiddleware = responseMiddleware;
