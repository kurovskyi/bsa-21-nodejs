const { user } = require('../models/user');

const {
  isNonEmptyObject,
  isExists,
  isValidInputKeys,
  isValidName,
  isValidGmail,
  isValidPhoneNumber,
  isValidPassword,
} = require('../utils/validation.utils');
const { ExtendedError } = require('../utils/error.utils');

const commonUserValid = (req, res, next, forUpdate = false) => {
  try {
    const inputData = req.body;

    if (
      !(
        isNonEmptyObject(inputData) &&
        isValidInputKeys({
          data: inputData,
          [forUpdate ? 'optionalKeys' : 'requiredKeys']: Object.keys(
            user
          ).filter((key) => key !== 'id'),
        })
      )
    )
      throw new Error(
        'Invalid User input object. Check that you have filled all fields.'
      );

    const { firstName, lastName, email, phoneNumber, password } = inputData;

    if ((!forUpdate || isExists(firstName)) && !isValidName(firstName))
      throw new Error(
        'Invalid User first name. It should contains only letters.'
      );
    if ((!forUpdate || isExists(lastName)) && !isValidName(lastName))
      throw new Error(
        'Invalid User last name. It should contains only letters.'
      );
    if ((!forUpdate || isExists(email)) && !isValidGmail(email))
      throw new Error(
        'Invalid User email. It should ends with "@gmail.com" and be correct.'
      );
    if (
      (!forUpdate || isExists(phoneNumber)) &&
      !isValidPhoneNumber(phoneNumber)
    )
      throw new Error('Invalid User phone number. Format: +380xxxxxxxxx');
    if ((!forUpdate || isExists(password)) && !isValidPassword(password))
      throw new Error('Invalid User password. Minimal length: 3');

    next();
  } catch (err) {
    res.status(400).send(ExtendedError.generateResponseError(err.message));
  }
};

const createUserValid = (...params) => commonUserValid(...params);

const updateUserValid = (...params) => commonUserValid(...params, true);

module.exports = {
  createUserValid,
  updateUserValid,
};
