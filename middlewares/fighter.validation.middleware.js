const { fighter } = require('../models/fighter');

const {
  isNonEmptyObject,
  isExists,
  isValidInputKeys,
  isValidName,
  isInRange,
} = require('../utils/validation.utils');
const { ExtendedError } = require('../utils/error.utils');

const commonFighterValid = (req, res, next, forUpdate = false) => {
  try {
    const inputData = req.body;

    if (
      !(
        isNonEmptyObject(inputData) &&
        isValidInputKeys({
          data: inputData,
          optionalKeys: ['health'],
          [forUpdate ? 'optionalKeys' : 'requiredKeys']: Object.keys(
            fighter
          ).filter(
            (key) => !['id', ...(!forUpdate ? ['health'] : [])].includes(key)
          ),
        })
      )
    )
      throw new Error(
        'Invalid Fighter input object. Check that you have filled all fields.'
      );

    const { name, health = 100, power, defense } = inputData;

    if (!req.body.health) req.body.health = health;

    if ((!forUpdate || isExists(name)) && !isValidName(name))
      throw new Error('Invalid Fighter name. It should contains only letters.');
    if ((!forUpdate || isExists(health)) && !isInRange(health, 81, 119))
      throw new Error('Invalid Fighter health. It should be > 80 and < 120.');
    if ((!forUpdate || isExists(power)) && !isInRange(power, 2, 99))
      throw new Error('Invalid Fighter power. It should be > 1 and < 100.');
    if ((!forUpdate || isExists(defense)) && !isInRange(defense, 2, 99))
      throw new Error('Invalid Fighter defense. It should be > 1 and < 10.');

    next();
  } catch (err) {
    res.status(400).send(ExtendedError.generateResponseError(err.message));
  }
};

const createFighterValid = (...params) => commonFighterValid(...params);

const updateFighterValid = (...params) => commonFighterValid(...params, true);

module.exports = {
  createFighterValid,
  updateFighterValid,
};
