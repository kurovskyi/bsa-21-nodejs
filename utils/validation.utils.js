const isNonEmptyObject = (data) => {
  if (data && typeof data === 'object' && Object.keys(data).length) return true;
  return false;
};

const isString = (data) => {
  if (typeof data === 'string') return true;
  return false;
};

const isExists = (data) => {
  if (data !== undefined && data !== null) return true;
  return false;
};

const isInRange = (data, min, max) => {
  if (typeof data === 'number' && data >= min && data <= max) return true;
  return false;
};

const isValidName = (data) => {
  const result = /^[a-z,.'-]+$/i.test(data);

  return result;
};

const isValidGmail = (data) => {
  const result = /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/.test(data);

  return result;
};

const isValidPhoneNumber = (data) => {
  const result = /^\+380\d{9}$/.test(data);

  return result;
};

const isValidPassword = (data) => {
  if (isString(data) && data.length >= 3) return true;
  return false;
};

const isValidInputKeys = ({ data, requiredKeys = [], optionalKeys = [] }) => {
  const inputKeys = Object.keys(data);
  const isValidKeys = inputKeys.every((key) =>
    [...requiredKeys, ...optionalKeys].includes(key)
  );
  const isRequiredKeysExists = requiredKeys.every((key) =>
    inputKeys.includes(key)
  );
  const result = isValidKeys && isRequiredKeysExists;

  return result;
};

module.exports = {
  isNonEmptyObject,
  isString,
  isExists,
  isInRange,
  isValidName,
  isValidGmail,
  isValidPhoneNumber,
  isValidPassword,
  isValidInputKeys,
};
