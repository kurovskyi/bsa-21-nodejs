class ExtendedError extends Error {
  isNotFound = false;

  constructor(message, isNotFound) {
    super(message);
    this.isNotFound = isNotFound;
  }

  static generateResponseError = (message = 'Not Found') => {
    const error = {
      error: true,
      message,
    };

    return error;
  };
}

module.exports = {
  ExtendedError,
};
